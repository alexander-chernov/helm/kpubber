# kpubber

![Version: 0.0.4](https://img.shields.io/badge/Version-0.0.4-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: v0.0.2](https://img.shields.io/badge/AppVersion-v0.0.2-informational?style=flat-square)

Annotation service for tagging kubernetes nodes with their public ip

## How to install this chart

A simple install without adding repository

```console
helm install --repo https://charts.alekc.dev/ kpubber kpubber
```

Add Alekc public chart repo:

```console
helm repo add alekc https://charts.alekc.dev/
```

A simple install with default values:

```console
helm install -g alekc/kpubber
```

To install the chart with the release name `my-release`:

```console
helm install my-release alekc/kpubber
```

To install with some set values:

```console
helm install my-release alekc/kpubber --set values_key1=value1 --set values_key2=value2
```

To install with custom values file:

```console
helm install my-release alekc/kpubber -f values.yaml
```

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` |  |
| config.annotationKeys | list | `["kilo.squat.ai/force-endpoint","flannel.alpha.coreos.com/public-ip-overwrite"]` | Define keys which should be added to the node containing the public ip |
| config.cron.enabled | bool | `false` | If set to true, then enables periodic refresh of the ip (useful in case you have dynamic ip) |
| config.cron.refreshInterval | string | `"@every 5m"` | Cron format used by the Quartz Scheduler. Depends on the config.cron.enabled |
| dns.config | object | `{"nameservers":["8.8.8.8","1.1.1.1"]}` | `dnsConfig` values |
| dns.enableOverride | bool | `true` | Enable dns override for the pod configuration |
| dns.policy | string | `"ClusterFirst"` | Dns policy |
| fullnameOverride | string | `""` |  |
| image.pullPolicy | string | `"IfNotPresent"` |  |
| image.repository | string | `"ghcr.io/alekc/kpubber"` |  |
| image.tag | string | `""` | Overrides the image tag whose default is the chart appVersion. |
| imagePullSecrets | list | `[]` |  |
| nameOverride | string | `""` |  |
| nodeSelector | object | `{}` |  |
| podAnnotations | object | `{}` |  |
| podSecurityContext | object | `{}` |  |
| replicaCount | int | `1` |  |
| resources.requests.cpu | string | `"100m"` |  |
| resources.requests.memory | string | `"32Mi"` |  |
| securityContext.capabilities.drop[0] | string | `"ALL"` |  |
| securityContext.readOnlyRootFilesystem | bool | `true` |  |
| securityContext.runAsNonRoot | bool | `true` |  |
| securityContext.runAsUser | int | `1000` |  |
| serviceAccount.annotations | object | `{}` | Annotations to add to the service account |
| serviceAccount.create | bool | `true` | Specifies whether a service account should be created |
| serviceAccount.name | string | `""` | The name of the service account to use. If not set and create is true, a name is generated using the fullname template |
| tolerations[0].effect | string | `"NoSchedule"` |  |
| tolerations[0].key | string | `"node-role.kubernetes.io/master"` |  |
| tolerations[0].operator | string | `"Exists"` |  |

